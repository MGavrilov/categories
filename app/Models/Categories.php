<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Categories extends Model
{
    use HasFactory;

    protected $fillable = [
        'title', 'description'
    ];

    public function categories(): HasMany
    {
        return $this->hasMany(__CLASS__, 'parent_id');
    }
}
