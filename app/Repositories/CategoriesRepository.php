<?php

namespace App\Repositories;

use App\Models\Categories;
use Illuminate\Database\Eloquent\Collection;

class CategoriesRepository
{
    public function all(): Collection
    {
        return Categories::query()->whereNull('parent_id')->get();
    }
}
