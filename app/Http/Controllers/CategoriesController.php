<?php

namespace App\Http\Controllers;

use App\Http\Resources\CategoriesResource;
use Illuminate\Http\JsonResponse;
use App\Services\CategoriesService;

class CategoriesController extends Controller
{
    public function index(CategoriesService $categoriesService): JsonResponse
    {
        $categories = $categoriesService->categories();
        return new JsonResponse(CategoriesResource::collection($categories));
    }
}
