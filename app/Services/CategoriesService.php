<?php

namespace App\Services;

use App\Repositories\CategoriesRepository;
use Illuminate\Database\Eloquent\Collection;

class CategoriesService
{
    private CategoriesRepository $categoryRepository;

    public function __construct(CategoriesRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function categories(): Collection
    {
        return $this->categoryRepository->all();
    }
}
